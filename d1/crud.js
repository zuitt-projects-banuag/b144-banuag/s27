const http = require('http');

//Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]
http.createServer((request, response) => {
	//Route for returning all items upon receiving a GET request
	if(request.url == '/users' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type': 'application/json'})
		response.write(JSON.stringify(directory));
		/*
			- When information is sent to the client, it is sent in the format of a stringified JSON.

			- After the client received the stringified JSON, then it is converted back into json object to be consumed.
		*/
		response.end()
	}

	if(request.url == '/users' && request.method == 'POST'){
		//Declare and initialize "requestBody" variable to an empty string
		//This will act as a placeholder for the resources/data to be created later on
		let requestBody = '';

		//Stream +> it is a sequence of data
		// 1. Data is received from the client and is processed in the 'data' scream called 'data' the code below will be triggered
		//data step - this reads the 'data' stream and process it as the request body
		request.on('data', function(data){
			//assign the data retrieved from the data stream to requestBody
			requestBody += data;
		})

		// 2. end step - only runs after the request has completely been sent (response once the data is already processed)
		request.on('end', function(){ 
			//check if at this point the requestBody is of data type STRING
			//We need this to be of data type json to access its properties
			console.log(typeof requestBody)

			//Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody)

			//Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			//response - stringify
			//request - parse 
			}

			//Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end()
		});
	}

}).listen(4000)

console.log('Server running at localhost:4000')