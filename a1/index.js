const http = require('http');

//Mock Database
let userInfo = [
	{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
	}
]

http.createServer((request, response) => {

	if(request.url == '/profile' && request.method == 'GET'){
		response.writeHead(200,{'Content-Type': 'application/json'})
		response.write(JSON.stringify(userInfo));
		

		response.end()
	}

	if(request.url == '/newUsers' && request.method == 'POST'){

		let newItem = '';

		request.on('data', function(data){
			newItem += data;
			});

		request.on('end', function(){ 
			console.log(typeof newItem)

		newItem = JSON.parse(newItem)

		let newUserInfo = {
				"firstName": newItem.firstName,
				"lastName": newItem.lastName,
				"mobileNo": newItem.mobileNo,
				"email": newItem.email,
				"password": newItem.password
			}
			userInfo.push(newUserInfo);
			console.log(userInfo);

		response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUserInfo));
			response.end()
		});

	}

}).listen(4000)


console.log('Server running at localhost:4000')